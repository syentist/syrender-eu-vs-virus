# SyRENDER #

SyRENDER (Scientific Research Experimental Network for Data Enhanced Rendering) is a virtual render farm 
dedicated to produce detailed 3D models and data analysis of scientific challenges, including COVID-19 imagery and epidemiology.